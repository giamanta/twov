#!/usr/bin/python
#
# pep8 & pyflakes validated
#
# Copyright (C) 2014  Giacomo Mantani
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

import zlib
from cStringIO import StringIO
import sys
import argparse


# Command line parser
def parse_cli():
    """Reads sys.argv and returns an options object."""
    parser = argparse.ArgumentParser(description='Decompress zlib archive')
    parser.add_argument("archive", help="Zlib compress data file")
    return parser.parse_args()


def main(args):
    data = []
    try:
        f = open(args.archive, 'rb')
    except IOError:
        sys.exit(1)
    while 1:
        d = f.read(4096)
        if d:
            data.append(d)
        else:
            break
    print StringIO(zlib.decompress(''.join(data))).readlines()
if __name__ == '__main__':
    main(parse_cli())

# EOF vim: set ts=4 sw=4 tw=79 :
