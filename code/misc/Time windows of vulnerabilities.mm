<map version="docear 1.1" project="1490EE02FA592Z8CFS4YC6OZ6L0PZD1455I3" project_last_home="file:/home/jack/Docear/projects/thesis" dcr_id="1413294135601_1u7bmll99g5cqc3kd6kijv210">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="Time windows of vulnerabilities" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1414516428595"><hook NAME="MapStyle" background="#ffffff">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<node TEXT="Cruft tool" LOCALIZED_STYLE_REF="defaultstyle.floating" FOLDED="true" POSITION="right" ID="ID_1123217999" CREATED="1413307237804" MODIFIED="1414101755903" HGAP="52" VSHIFT="-80" MOVED="1413307249996" LINK="https://alioth.debian.org/projects/cruft/">
<hook NAME="FreeNode"/>
<node TEXT="cruft is a program to look over the system for anything that shouldn&apos;t&#xa;be there, but is; or for anything that should be there, but isn&apos;t." ID="ID_1923505774" CREATED="1413307273633" MODIFIED="1413307277380"/>
</node>
<node TEXT="Input" POSITION="right" ID="ID_1269952722" CREATED="1413294273950" MODIFIED="1414177204387" HGAP="30" VSHIFT="10">
<edge COLOR="#ff0000"/>
<node TEXT="System Logs" ID="ID_873880678" CREATED="1413294368896" MODIFIED="1414101732132" HGAP="50" VSHIFT="-10">
<node TEXT="dpkg.log format" ID="ID_1193847066" CREATED="1413296202009" MODIFIED="1414099124478"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      /var/log/dpkg.log.* does not contains software that
    </p>
    <p>
      has been self-compiled and installed using 'make install'
    </p>
    <p>
      There are tools that help track these informations, like
    </p>
    <p>
      checkinstall and stow, is these one available?
    </p>
  </body>
</html>
</richcontent>
<node TEXT="if rotated -&gt; dpkg.log.$COUNT" ID="ID_329649651" CREATED="1414099124515" MODIFIED="1414099239102"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      eventually old /var/log/dpkg.log.* files
    </p>
    <p>
      will be deleted by log rotation, so that
    </p>
    <p>
      way isn't guaranteed to give you the
    </p>
    <p>
      entire history of your system.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="if a package was never upgraded or /var/log/dpkg.log&#xa;is not available, we could get info from /var/lib/dpfk/info/*.list" ID="ID_975572659" CREATED="1414099349506" MODIFIED="1414100510612" LINK="http://unix.stackexchange.com/questions/12578/list-packages-by-installation-date">
<node TEXT="inside these files, there are not more&#xa;information other than package name&#xa;and installation(creation) date. Escamotage:&#xa;We could suppose that if there is no log file about&#xa;updates, the actual package is the one in **/info/*.list" ID="ID_974373650" CREATED="1414177255219" MODIFIED="1414178959930"/>
</node>
</node>
<node TEXT="Remote advisory" ID="ID_469508789" CREATED="1413295739688" MODIFIED="1414177213880" HGAP="50" VSHIFT="60">
<node TEXT="Debian secure-testing repository (MAIN)" ID="ID_1498971221" CREATED="1413306751728" MODIFIED="1413306924807" MOVED="1413306761184" LINK="http://testing-security.debian.net/">
<node TEXT="Starting point? debsecan" ID="ID_1876410683" CREATED="1413306857783" MODIFIED="1413580507778" LINK="http://www.enyo.de/fw/software/debsecan/"/>
</node>
<node TEXT="Debian Security Advisory" ID="ID_1762785550" CREATED="1413297678865" MODIFIED="1413310032319" LINK="https://www.debian.org/security/dsa-long"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      RDF format
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Debian Security Tracker" ID="ID_1632719492" CREATED="1413300351185" MODIFIED="1413306956976" LINK="https://security-tracker.debian.org/tracker/"/>
<node TEXT="Bits from Security team" FOLDED="true" ID="ID_122771711" CREATED="1413307337083" MODIFIED="1413307375196" LINK="https://lists.debian.org/debian-devel-announce/2014/03/msg00004.html">
<hook NAME="FirstGroupNode"/>
<node TEXT="See #debian-security at irc.debian.org" ID="ID_1298733933" CREATED="1413307377609" MODIFIED="1413307417061"/>
<node TEXT="Titan pad" ID="ID_1305414597" CREATED="1413307434581" MODIFIED="1413307446904" LINK="https://titanpad.com/secteamessen2014"/>
</node>
</node>
</node>
<node TEXT="Output" POSITION="left" ID="ID_164803637" CREATED="1413702108602" MODIFIED="1414177196715" HGAP="50" VSHIFT="20">
<edge COLOR="#0000ff"/>
<node TEXT="CLI like" ID="ID_1393424873" CREATED="1413702119428" MODIFIED="1413702277088" MOVED="1413702282214"/>
<node TEXT="JSON" ID="ID_1168889229" CREATED="1413702286111" MODIFIED="1413702339923" LINK="http://bl.ocks.org/mbostock/raw/4063550/flare.json">
<node TEXT="Rapresented in HTML" ID="ID_867979602" CREATED="1413702344437" MODIFIED="1413702392448" LINK="http://bl.ocks.org/mbostock/4339083"/>
</node>
</node>
<node TEXT="Helpful diagrams" POSITION="left" ID="ID_110390050" CREATED="1414516077884" MODIFIED="1414516093159">
<edge COLOR="#ff00ff"/>
<node TEXT="manteiners scripts UML" ID="ID_1618384025" CREATED="1414516093280" MODIFIED="1414516166325" LINK="https://people.debian.org/~srivasta/MaintainerScripts.html"/>
<node TEXT="B/W manteiners scripts UML" ID="ID_1992302057" CREATED="1414516157803" MODIFIED="1414516203521" LINK="https://wiki.debian.org/MaintainerScripts"/>
</node>
</node>
</map>
