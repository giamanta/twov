cat /var/log/dpkg.log* | \
  grep -e '.* install .*' | \
  # -k start_column,end_column
  sort -u -t' ' -k4,4 > filetered_lines
