' Copyright (C) 2014  Giacomo Mantani
'
' This program is free software; you can redistribute it and/or
' modify it under the terms of the GNU General Public License
' as published by the Free Software Foundation; either version 2
' of the License, or (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program; if not, write to the Free Software
' Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
' USA.

@startuml
title
 Get <i>CVEs</i> <u>merging</u> information
 from <i>CVE/list</i> and MITRE all items
 archive
end title
participant "Tool"
participant "Viewvc secure-testing"
participant "cve.mitre.org"

note over "Viewvc secure-testing"
      updated two times a day
      from internal tracker DB
end note

note over "cve.mitre.org"
      complete CVEs archive
end note
activate "Tool" #Darksalmon
"Tool" -> "Viewvc secure-testing": Request "CVE/list"
activate "Viewvc secure-testing" #DarkSalmon

"Viewvc secure-testing" -> "Tool": Return list
deactivate "Viewvc secure-testing"

"Tool" -> "Tool": Parse Information
activate "Tool" #coral
deactivate "Tool"

"Tool" -> "cve.mitre.org": Request CVEs archive
activate "cve.mitre.org" #Darksalmon
"cve.mitre.org" -> "Tool": Return archive
deactivate "cve.mitre.org"

"Tool" -> "Tool": Parse Information
activate "Tool" #coral
deactivate "Tool"

"Tool" -> "Tool": Merge Information
[<-[#white]- "Tool":
deactivate "Tool"
rnote over "Tool"
      Build a complete CVEs
      list related to Debian with
      release date for each ID
endrnote

@enduml
' EOF vim: set ts=2 sw=2 tw=80 :
