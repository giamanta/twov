Licenza:
--------
Copyright (C) 2014  Giacomo Mantani

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


Title:
------

    Individuazione delle “finestre di vulnerabilità” di una specifica
    distribuzione / installazione (Time windows of vulnerabilities).

Descrizione di massima:
-----------------------

    l’obiettivo di questo lavoro è quello di progettare una metodologia e
    costruire uno strumento per la consultazione automaticamente dei
    principali database di vulnerabilità del software ed incrociare le
    informazioni ottenute con i changelog di una specifica distribuzione.
    Questa operazione permette di determinare quando e come una ben
    determinata installazione è risultata affetta da specifiche
    vulnerabilità. Le informazioni ottenute hanno una valenza fondamentale
    nella determinazione delle responsabilità del possessore /
    amministratore del sistema analizzato.

    Stima qualitativa e temporale, per determinare in un eventuale analisi
    forense, cosa ha (o potrebbe) aver sfruttato un attaccante.
    Bisogna cercare di definire anche un livello minimo di sicurezza, se i
    changelog sono sulla macchina attaccata, quanto possono essere realmente
    utilizzati? Se son su di un server esterno, esso quanto è sicuro dall'
    attaccante? Le finestre di vulnerabilità indicano proprio per quanto una
    distribuzione era vulnerabile ad un determinato 'bug' e come esso possa
    esser stato sfruttato per compromettere altre eventuali falle.

    NOTA: attività di ricerca in collaborazione con Stefano Zacchiroli.

Lapsus:
-------
  Per quanto riguarda l'applicativo sviluppato, se si sta visionando una
  versione diversa da quella presente nella repository ufficiale, è possibile
  siano presenti errori o parti non del tutto complete.
  Accedere alla repository pubblica in rete per visionare il lavoro allo stato
  corrente attuale.

<!-- EOF vim: set ts=2 sw=2 tw=80 : -->
