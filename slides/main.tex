\documentclass{beamer}

% Copyright (C) Giacomo Mantani
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

\mode<presentation>
{
  \usetheme{Marburg}
  %\useoutertheme{infolines}

  %\setbeamertemplate{background canvas}[vertical shading][bottom=structure.fg!10,top=structure.fg!25]
  \setbeamercovered{invisible}
}

\usepackage[italian]{babel}
\usepackage[fixlanguage]{babelbib}
\selectbiblanguage{italian}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{times}
\usepackage{listings,graphicx,adjustbox}
\usepackage{xcolor}
\definecolor{kw}{RGB}{26, 35, 62}%95, 131, 240}%38,67,154}
\definecolor{kwDark}{RGB}{47,64,115}
\definecolor{kwLight}{RGB}{21,66,201}
\definecolor{grey}{RGB}{43,51,76}

\title[] % (optional)
{Finestre di Vulnerabilità e Pacchetti Software}

%\subtitle
%{Presentation Subtitle}

\author[] % (optional)
{Giacomo Mantani}
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Alma Mater Studiorum] % (optional)
{
  %\inst{1}%
  Scienze e Tecnologie Informatiche\\
  Università di Bologna\\
  Campus di Cesena
}
% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

\date[Short Occasion] % (optional)
{11 Dicembre 2014}

\subject{Tesi}
% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command:

%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Sommario}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}

\section{Obiettivo}
\begin{frame}{Obiettivo}
  Sviluppare uno strumento per analizzare la storia delle
  \textcolor{kwLight}{vulnerabilità} associate ai \textcolor{kwLight}{pacchetti}
  software in un sistema operativo GNU/Linux Debian.\\
  \vspace{5mm}
  \centering
  \includegraphics[scale=.5]{imgs/openlogo.eps}
\end{frame}

\section{Applicazione}
\begin{frame}{Dove può essere applicato?}
  \begin{tabular}[c]{l p{5cm}}
    \textcolor{kwLight}{\emph{Computer Forensics}}: & disciplina che collega Legge ed informatica per
    analizzare dati da presentare alla Corte giudiziaria
  \end{tabular}
  \begin{adjustbox}{scale=0.4}
    \lstinputlisting[basicstyle=\ttfamily]{magnify2}
  \end{adjustbox}
\end{frame}

\section{Vantaggi}
\begin{frame}{Che vantaggi porta?}
  \begin{enumerate}
      \item
        Individuare intervalli di tempo in cui si è stati potenzialmente esposti ad attacchi
      \item
        Conoscere la storia per evitare di ripetere errori nel futuro
      \item
        Analizzare i tempi di aggiornamento da parte della distribuzione
        (Debian) e dell'amministratore
  \end{enumerate}
\end{frame}

\section{Parole Chiave}
\begin{frame}{Parole Chiave}
  \begin{itemize}
  \item
    \textcolor{kwLight}{\emph{Computer Forensics}}
  \item
    \textcolor{kwLight}{Standard}
  \item
    \textcolor{kwLight}{Pacchetto} Software
  \item
    \textcolor{kwLight}{Vulnerabilità}: problema legato alla sicurezza di un sistema
  \end{itemize}
\end{frame}

\section{Cosa esiste già?}
\begin{frame}{Cosa esiste già?}
  \begin{tabular}[c]{p{2.5cm} p{6.1cm}}
      \emph{debsecan}: & \emph{tool} che valuta lo stato di un \emph{host}
      evidenziando se sono presenti \textcolor{kwLight}{vulnerabilità} ed aggiornamenti tra i
      software installati \alert{senza permettere analisi di periodi passati}
  \end{tabular}
\end{frame}

\section{Come è stato realizzato?}
\subsection{Ottenere le informazioni}
\begin{frame}{Come è stato realizzato?}
  Per prima cosa ho ottenuto le informazioni:
  \begin{itemize}
    \item dai file del \emph{package manager} \textcolor{kwDark}{dpkg}:
      \begin{itemize}
        \item  \texttt{/var/lib/dpkg/status}
        \item  \texttt{/var/log/dpkg.log}
      \end{itemize}
    \item da \emph{advisory} remote (descritte da \textcolor{kwLight}{standard}):
      \begin{itemize}
        \item Debian -> \textcolor{kwDark}{DSA}\footnotemark,
            \textcolor{kwDark}{CVE}\footnotemark
        \item MITRE -> \textcolor{kwDark}{CVE}\footnotemark
      \end{itemize}
  \end{itemize}
  Per riuscire ad utilizzarle è stato necessario elaborarne il contenuto,
  effettuando tecniche di \emph{parsing}, al momento basate su \emph{Regular
    Expression} in cascata.

\footnotetext[1]{Debian Security Advisory}
\footnotetext[2]{CVE modificati dal team di sicurezza Debian}
\footnotetext[3]{Common Vulnerabilities \& Exposures}
\end{frame}

\subsection{Creazione della storia}
\begin{frame}{}
  Una volte ottenute queste informazioni ho dovuto ricostruire il passato, utilizzando
  il seguente algoritmo:
  \begin{itemize}
    \item Caricamento dello stato corrente
    \item Selezione dei log che riguardano l'intervallo di tempo preso in
        esame
      \item Costruzione per ogni \textcolor{kwLight}{pacchetto} software della sua storia (finestra
        temporale in cui era/è presente nel sistema)
  \end{itemize}
  \begin{figure}[b]
    \centering
    \includegraphics[width=0.8\linewidth]{imgs/stories.eps}
  \end{figure}
  \begin{figure}[b]
    \centering
    \includegraphics[width=0.8\linewidth]{imgs/compact.eps}
  \end{figure}
\end{frame}

\subsection{Creazione Finestre di Vulnerabilità}
\begin{frame}[fragile]
  Costruita la storia per ogni \textcolor{kwLight}{pacchetto} software all'interno dell'intervallo di
  tempo interessato, ho associato ad ogni periodo le \textcolor{kwLight}{vulnerabilità} ad esso
  collegate.\\
  \pause
  \vspace{5mm}
  In questo modo si è venuta a formare la prima versione di un nuovo
  \emph{tool} che ho denominato \textbf{Lapsus}.
  \begin{verbatim}
               _.--.
           .-"'_.--.\   .-.___________
         ."_-"'     \\ (  o;------/\"''
       ,."=___      =)) \ \      /  \
        '~' .='~'~)  ( _/ /     /    \
='---====""~'\          _/     /      \
              '-------"'      /        \
                             /          \
                            (   Lapsus   )
                             '._      _.'
                                '----'
  \end{verbatim}
\end{frame}

\section{Riepilogo}
\begin{frame}{Riepilogo}
  \begin{itemize}
  \item
    \textcolor{kw}{\textbf{Lapsus}}, è:
    \begin{enumerate}
      \item \emph{open source}
      \item rilasciato sotto licenza \textcolor{kw}{\textbf{GPL}} versione 2
      \item scritto in \textcolor{kw}{\textbf{Python}} e compatibile con gli standard
        \textcolor{kw}{\textbf{pep8}}, \textcolor{kw}{\textbf{pyflakes}}
    \end{enumerate}
  \item
    I risultati ottenuti si basano su \textcolor{kw}{\textbf{informazioni}} che possono essere
    state a loro volta \textcolor{kw}{\textbf{compromesse}} ed è opportuno tenerne conto
  \item
    Provatelo! I \textcolor{kw}{\textbf{feedback}} sono sempre bene accetti!
  \end{itemize}
\end{frame}

\end{document}
%EOF vim: set ts=2 sw=2 tw=80 :
